import ply.yacc as yacc
import ply.lex as lex
import sys
import lexer
from lexer import tokens
import string
#
# asm-dcpu-16 parser
#
# data_section = { labelname : [hexdata1, hexdata2, ....],...}
data_labels = []
data = []
all_data = {}
LAST = 0
DATA_NUM = 0


# AST Format:
#----------------
# [(OPNAME, OPCODE, Cycles, Function), Operand1, Operand2]
# [("LABEL",0x00,None,"op_LABEL"),p[0],None]
# Labels: 
#       have the Ast entry aboce, where Operadnd1=p[0]=Label Name.
#       If the Operand2 == DATA, then this label is labeling the Data_section
# Unary Operations:
#       Operations with just one Operand have Operand2 = None
# Binary Operation: 
#       p[2] is in ["+","-","*","/"]
#       operartion is in ["PLUS","MINUS", "TIMES", "DIVIDE","ERROR"]
#       p[1] and p[3] are Operand1 and Operand2
#       [(p[2],0x00,0,"op_"+operation), p[1], p[3]]

ast =  []

# initialize the symbol table
symbol_table = []

# initialize the registers
registers = {}
for reg in lexer.register:
    registers[reg] = 0x0

opcodes = lexer.opcodes
special_opcodes = lexer.special_opcodes

# set the memory start
MEM_START = 0x00
MODULO_SIZE = 0xffff

def _dump_regs():
    print "registers:"
    print 40*"-"
    for elem in registers.keys():
        # print "%03d - |%-7s|" % (no, txt)
        print "%20s %8s : %-6s" % (" ", elem, registers[elem])

def _dump_syms(): 
    print "symbol_table:"
    print 40*"-"
    for elem in symbol_table:
        # print "%03d - |%-7s|" % (no, txt)
        print "%20s %-6s" % (" ", elem)

def _dump_ast():
    print "abstract syntax tree:"
    print 10*" ",
    print " idx   op    opcode   cyc     op1     op2 "
    print 60*"-"
    counter = 0 
    for elem in ast:
        # print "%03d - |%-7s|" % (no, txt)
        print "%10s %4s %6s  %6s  %4s  %6s  %6s" %  (" ", counter, elem[0][0], hex(elem[0][1]), 
                                                            elem[0][2], elem[1], elem[2])
        counter += 1

def _dump_data():
    print "data section:", data_label
    print 30*"-"
    counter = 0 
    for section in data_sections.keys():
        print "Data section: ", section
        print "%20s" % (" "),
        for elem in data_sections[section]:
            print " %6s" % (hex(elem)),
            counter += 1 
            if counter == 7:
                counter = 0
                print 
                print "%20s" % (" "),
    print

def _dump_info():
    _dump_regs()
    _dump_syms()
    _dump_ast()

precedence = ( 
        ('left','PLUS','MINUS'), 
        ('left','TIMES','DIVIDE')
        ) 

def p_program(p):
    """program      : statement_list"""
    print "dcpu-16 Assembler Program found."
    print 40*"-"
    print " -> final AST:"
    _dump_ast()
    print 
    print " -> Final DATA_Section:"
    #_dump_data()
    print 
    print " -> Final Symbol table:"
    _dump_syms()
    print data
    sys.exit()


def p_statement_list(p):
    """statement_list   :   empty
                        |   statement 
                        |   statement statement_list"""
    #print "PARSER: statement_list"
    pass

def p_empty(p):
    """empty :"""
    pass 

def p_statement(p):
    """statement    : newline
                    | label
                    | dat_statement
                    | set_statement
                    | if_statement
                    | bor_statement
                    | add_statement
                    | and_statement
                    | shl_statement
    """
    print "PARSER: found statement: %s " % (str(p[1]))


def p_newline(p):
    """newline      :   NEWLINE"""
    #print "PARSER: found: NEWLINE"
    p[0] = "NEWLINE"

def p_label(p):
    """ label       : LABEL"""
    print "PARSER: found label: %s" % (str(p[1]))
    symbol_table.append(p[1])
    p[0] = p[1]
    ast.append([("LABEL",0x00,None,"op_LABEL"),p[0],None])


def p_dat_statement(p):
    """dat_statement    : DAT datalist"""
    #pseudo opcode DAT => http://0x10cwiki.com/wiki/DAT
    # get the label for this data section
    data_lab = symbol_table[-1]
    counter = 0
    # now go and update the label in the AST.
    # Mark it with DATA to link it to a data_section
    for elem in ast:
        if elem[0][0] == "LABEL" and elem[1] == data_lab:
            elem[2] = "DATA"
        counter += 1
    # Add the current data_section to the data_sections dict
    data_labels.append(data_lab)
    print "Dat statement. Label =", data_labels
    LAST = len(data)
    print "LAST: ", LAST



def p_datalist_list(p):
    """datalist     :   empty
                    |   data
                    |   data COMMA datalist"""
    print "PARSER: datalist"

def p_data(p):
    """data         :   HEXNUM 
                    |   STRING"""
    if type(p[1]) == str:
        # if its a string, convert it to INT-ASCII, char by char
        for char in list(p[1]):
            data.append(ord(char))
    else:
        data.append(int(p[1]))
    print "PARSER: datalist", p[1]


def p_add_statement(p):
    """add_statement :  ADD REGISTER COMMA expression
                     |  ADD expression COMMA expression"""   
    #  2 | 0x02 | ADD b, a | sets b to b+a, sets EX to 0x0001 if there's an overflow, 
    ostr= str(p[1]) + " " + str(p[2]) + str(p[3]) + str(p[4])
    print "PARSER: AND Statement: ", ostr
    try: 
        # set the value of the register
        res = registers[p[2]] + p[4]
        if res % MODULO_SIZE > 0:
            registers["ex"] = 0x0001
        registers[p[2]] = registers[p[2]] + p[4]
    except LookupError: 
        print "Undefined Register name '%s' should never happen"
        sys.exit(0)
    print _dump_ast()
    ast.append([opcodes.get("add"), p[2],p[4]])

def p_and_statement(p):
    """and_statement :  AND REGISTER COMMA expression
                     |  AND expression COMMA expression"""   
    #2 | 0x02 | ADD b, a | sets b to b+a, sets EX to 0x0001 if there's an overflow, 
    #  |      |          | 0x0 otherwise
    ostr= str(p[1]) + " " + str(p[2]) + str(p[3]) + str(p[4])
    print ostr
    print "PARSER: AND Statement: ", ostr
    ast.append([opcodes.get("and"), p[2], p[4]])
    print _dump_ast()

def p_shl_statement(p):
    """shl_statement :  SHL REGISTER COMMA expression"""   
    # 1 | 0x0f | SHL b, a | sets b to b<<a, sets EX to ((b<<a)>>16)&0xffff
    ostr= str(p[1]) + " " + str(p[2]) + str(p[3]) + str(p[4])
    print ostr
    print "PARSER: AND Statement: ", ostr
    ast.append([opcodes.get("shl"), p[2], p[4]])
    print _dump_ast()


def p_bor_statement(p):
    """bor_statement :  BOR REGISTER COMMA expression
    """   
    #  1 | 0x0b | BOR b, a | sets b to b|a      
    ostr= str(p[1]) + " " + str(p[2]) + str(p[3]) + str(p[4])
    print "PARSER: BOR Statement: ", ostr
    ast.append([opcodes.get("bor"), p[2], p[4]])
    print _dump_ast()



def p_set_statement(p):
    """set_statement    : SET REGISTER COMMA expression
                        | SET expression COMMA expression
    """         
    ostr= str(p[1]) + " " + str(p[2]) + str(p[3]) + str(p[4])
    print ostr
    print "PARSER: SET Statement: ", ostr
    ast.append([opcodes.get("set"), p[2], p[4]])
    print _dump_ast()

def p_if_statement(p):
    #    p[0]          p[1]  p[2]    p[3]   p[4]
    """if_statement  :  IFB REGISTER COMMA expression
                    |  IFC REGISTER COMMA expression
                    |  IFE REGISTER COMMA expression
                    |  IFN REGISTER COMMA expression
                    |  IFG REGISTER COMMA expression
                    |  IFA REGISTER COMMA expression
                    |  IFL REGISTER COMMA expression
                    |  IFU REGISTER COMMA expression
    """         
    ostr = str(p[1]) + " " + str(p[2]) + str(p[3]) + str(p[4])
    print "PARSER: IF Statement: %s  %s, %s " % ( p[1], p[2], p[4] ),
    ifop = string.upper(p[1])
    ast.append([opcodes.get(p[1]), p[2], p[4]])
    
    


def p_expression_singleop(p):
    """expression   :   number
                    |   hexnum
                    |   LBRACKET expression RBRACKET
    """
    if len(p) == 2:
        p[0] = p[1]
    elif len(p) == 4:
        p[0] = p[2]
    #print "PARSER: p_expression_singleop found: %s" % p[0]

def p_expression_register(p):
    """expression   :   REGISTER """    
    try:
        p[0] = p[1]
    except LookupError: 
        print "Undefined Register ! Should never happen! Bailing out ..."
        sys.exit(0)
        


def p_expression_text(p):
    """expression   :   TEXT """    
    p[0] = p[1] 

def p_number(p):
    "number         : NUMBER"
    #print "PARSER: number ", p[1]
    p[0] = p[1]


def p_hexnum(p):
    "hexnum         :   HEXNUM"
    #print "PARSER: hexnum ", p[1]
    p[0] = p[1]


def p_expression_binop(p):
    """expression   :   expression PLUS expression
                    |   expression MINUS expression
                    |   expression TIMES expression
                    |   expression DIVIDE expression
    """
    #ast.append([(p[2],0x00,0,"op_PLUS"), p[1], p[3]])
    print "PARSER: expression_binop found", p[0], p[1], p[2], p[3]
    if p[2] == "+": 
        operation = "PLUS"
    elif p[2] == "-": 
        operation = "MINUS"
    elif p[2] == "*": 
        operation = "TIMES"
    elif p[2] == "/": 
        operation = "DIVIDE"
    else:
        operation = "ERROR"
    p[0] = [(p[2],0x00,0,"op_"+operation), p[1], p[3]]


def p_expression_group(p):
    """expression   :   LPAREN expression RPAREN
    """
    print "PARSER: p_expression_group found: %s"  % str(p)
    p[0] = p[2]
    


def p_error(p):
    print "Syntax error in input!",p 
    sys.exit(0)
    

if __name__ == "__main__":

    if len(sys.argv) < 2:
        print "usage from cli: python lexer.py <input_file.asm>"
        sys.exit()
    elif sys.argv[1] in ["-h", "--help", "/?"]:
        print "usage from cli: python lexer.py <input_file.asm>"
        sys.exit()
    
    filename = sys.argv[1]
    
    # Error rule for syntax errors
    # Build the parser
    #mylexer = lex.lex(module=lexer)
    myparser = yacc.yacc(debug=1)
    
    #print dir(parser)
    f = open(filename, "r")

    sometext = f.read()
    while True:
       try:
           s = sometext
       except EOFError:
           break
       if not s: continue
       result = myparser.parse(s)
       print result