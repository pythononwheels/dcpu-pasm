import ply.yacc as yacc
import ply.lex as lex
import sys
import badlang_lexer
from badlang_lexer import tokens
import string
import badlang_to_asm
import os
# simple Tree structure taken from http://www.dabeaz.com/ply/ply.html#ply_nn34
# used for (nested) expressions
tab_counter = 1

class Node:
    def __init__(self,type,children=None,leaf=None):
         self.type = type
         if children:
              self.children = children
         else:
              self.children = [ ]
         self.leaf = leaf
         Node.tabs = 1

    def __repr__(self):
        ostr = ""
        if self.type == "label":
            ostr += os.linesep + Node.tabs * "  " + " |-> "
            Node.tabs += 1
        elif self.type == "binop":
            ostr += os.linesep + Node.tabs * "  " + " |-> "
            Node.tabs += 1
        ostr += self.type + "(" + self.leaf + "," 
        counter = 1
        for elem in self.children:
            ostr += str(elem) 
            if counter < len(self.children):
                ostr += ","
            counter += 1
        return ostr + ")" 
        
#       
# asm-dcpu-16 parser
#
# data_sections 
data = []

# AST Format:
#----------------
# [ [OPERATION, OPerand1, Operand2 ], ... ]
ast =  []

# initialize the symbol table
# format: { scope : [Symbols] }                
# global scopes name is: global
symbol_table = {"global" : []}
scope = "global"
commands = badlang_lexer.commands

# modulo 16 bit
MODULO_SIZE = 0xffff

def _dump_data():
    print "data"
    print 40*"-"
    for elem in data:
        print 10*" ", elem

def _dump_syms(): 
    print "symbol_table:"
    print 40*"-"
    print "%15s %3s % 15s" % ("scope", " ", " Symbol Name")
    print 40*"-"
    for scope in symbol_table.keys():
        # print "%03d - |%-7s|" % (no, txt)
        for sym in symbol_table[scope]:
            print "%15s %3s %15s" % (scope, " ", sym)


def _dump_ast():
    print "abstract syntax tree:"
    print 40*"-"
    for elem in ast:
        #print 10*" ",
        print elem
        Node.tabs = 1

def _dump_info():
    _dump_syms()
    _dump_ast()

def append_to_symbol_table(name, scope):
    if name in symbol_table[scope]:
        err_msg  = "Symbol %s already defined for scope: %s" % (name, scope)
        raise Exception(err_msg)
        return False
    else:
        symbol_table[scope].append(name)
    return True

precedence = ( 
        ('left','PLUS','MINUS'), 
        ('left','TIMES','DIVIDE')
        ) 

def p_program(p):
    """program      : statement_list"""
    print "BADLang program found"
    print 
    print " -> Final Symbol table:"
    _dump_syms()
    print 
    print " -> Final Data section:"
    _dump_data()
    print 40*"-"
    print " -> final AST:"
    _dump_ast()
   
    sys.exit()


def p_statement_list(p):
    """statement_list   :   empty 
                        |   statement
                        |   lineno statement
                        |   statement statement_list
                        |   lineno statement statement_list"""
    #print "PARSER: statement_list"
    pass

def p_empty(p):
    """empty :"""
    pass 

def p_statement(p):
    """statement    : newline
                    | comment
                    | label
                    | print_statement
                    | goto_statement
                    | assign_statement
                    | def_statement
    """
    print "PARSER: found statement: %s " % (str(p[1]))


def p_label(p):
    """ label   : LABEL"""
    print "PARSER: Label: ", p[1]
    ast.append(["LABEL",p[1],None])

def p_lineno(p):
    """lineno      :   NUMBER"""
    #print "PARSER: found: NEWLINE"
    ast.append(["LABEL",p[1],None])


def p_newline(p):
    """newline      :   NEWLINE"""
    #print "PARSER: found: NEWLINE"
    p[0] = "NEWLINE"

#def p_label(p):
#    """ label       : LABEL"""
#    print "PARSER: found label: %s" % (str(p[1]))
#    symbol_table.append(p[1])
#    p[0] = p[1]
#    ast.append(["LABEL",p[0],None])


def p_comment(p):
    """comment :  COMMENT"""   
    #  2 | 0x02 | ADD b, a | sets b to b+a, sets EX to 0x0001 if there's an overflow, 
    ostr= str(p[1]) 
    print "PARSER: COMMENT: ", ostr
    ast.append(["COMMENT", p[1], None])

def p_def_statement(p):
    """def_statement :  DEF TEXT"""   
    #  2 | 0x02 | ADD b, a | sets b to b+a, sets EX to 0x0001 if there's an overflow, 
    ostr= str(p[1]) + " " + str(p[2]) 
    print "PARSER: DEF Statement: ", ostr
    append_to_symbol_table(p[2], scope)
    ast.append(["DEF", p[2], scope])


def p_goto_statement(p):
    """goto_statement   :  GOTO TEXT
                        |  GOTO NUMBER
    """   
    #  2 | 0x02 | ADD b, a | sets b to b+a, sets EX to 0x0001 if there's an overflow, 
    ostr= str(p[1]) + " " + str(p[2]) 
    print "PARSER: GOTO Statement: ", ostr
    ast.append(["GOTO", p[2], None])

def p_print_statement(p):
    """print_statement :  PRINT STRING"""   
    #  2 | 0x02 | ADD b, a | sets b to b+a, sets EX to 0x0001 if there's an overflow,
    print "PARSER: PRINT Statement: ", p[2]
    data.append([p[2]])
    ast.append(["PRINT", p[2], None])

def p_assign_statement(p):
    """assign_statement :  TEXT ASSIGN expression
                        |  TEXT ASSIGN STRING"""   
    #  2 | 0x02 | ADD b, a | sets b to b+a, sets EX to 0x0001 if there's an overflow, 
    ostr= str(p[1]) + " " + str(p[2]) + str(p[3]) 
    print "PARSER: ASSIGN Statement: ", ostr
    try:
        append_to_symbol_table(p[1],scope)
    except Exception as e:
        pass
    #            operation, symbol_table_link, expression
    ast.append(["ASSIGN", Node("label", [p[1]], ":"), p[3]])


def p_expression_singleop(p):
    """expression   :   number
                    |   hexnum
                    |   LBRACKET expression RBRACKET
    """
    if len(p) == 2:
        p[0] = p[1]
    elif len(p) == 4:
        p[0] = p[2]
    #print "PARSER: p_expression_singleop found: %s" % p[0]
    
def p_expression_text(p):
    """expression   :   TEXT """    
    p[0] = Node("label", [p[1]], ":")

def p_number(p):
    "number         : NUMBER"
    #print "PARSER: number ", p[1]
    p[0] =  Node("number", [p[1]], "#")


def p_hexnum(p):
    "hexnum         :   HEXNUM"
    #print "PARSER: hexnum ", p[1]
    p[0] = Node("number", [p[1]], "0x")


def p_expression_binop(p):
    """expression   :   expression PLUS expression
                    |   expression MINUS expression
                    |   expression TIMES expression
                    |   expression DIVIDE expression
    """
    #ast.append([(p[2],0x00,0,"op_PLUS"), p[1], p[3]])
    print "PARSER: expression_binop found", p[0], p[1], p[2], p[3]
    if p[2] == "+": 
        operation = "PLUS"
    elif p[2] == "-": 
        operation = "MINUS"
    elif p[2] == "*": 
        operation = "TIMES"
    elif p[2] == "/": 
        operation = "DIVIDE"
    else:
        operation = "ERROR"
    p[0] = Node("binop", [p[1],p[3]], p[2])
    #p[0] = ["op_"+operation, p[1], p[3]]


def p_expression_group(p):
    """expression   :   LPAREN expression RPAREN
    """
    print "PARSER: p_expression_group found: %s"  % str(p)
    p[0] = Node("group", [p[2]], "()")
    


def p_error(p):
    print "Syntax error in input!",p 
    sys.exit(0)
    
def generate_asm(ast, data, symbol_table):
    # compiles the given input to dcpu-16 asm
    asm = ""
    compiler = DlangToAsm(ast, data, symbol_table)
    counter = 0
    # loop over all instructions in ast
    for elem in ast:
        operation = elem[0]
        try:
            asm = asm + getattr(compiler, "op_" + operation)(asm, ats[counter])
        except Exception as e:
            print e 
            pass

        # next instruction
        counter += 1
    return asm


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print "usage from cli: python lexer.py <input_file.asm>"
        sys.exit()
    elif sys.argv[1] in ["-h", "--help", "/?"]:
        print "usage from cli: python lexer.py <input_file.asm>"
        sys.exit()
    
    filename = sys.argv[1]
    
    # Error rule for syntax errors
    # Build the parser
    mylexer = lex.lex(module=badlang_lexer)
    myparser = yacc.yacc(debug=1)
    
    #print dir(parser)
    f = open(filename, "r")

    sometext = f.read()
    while True:
       try:
           s = sometext
       except EOFError:
           break
       if not s: continue
       result = myparser.parse(s)
       print result
    print "hallo"
    asm = generate_asm(ast, data, symbol_table)
    print asm