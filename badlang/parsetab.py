
# parsetab.py
# This file is automatically generated. Do not edit.
_tabversion = '3.2'

_lr_method = 'LALR'

_lr_signature = '\xc3\xf6 \x0c,\x88\xeeb\x9eE\xec.\x82\xa5\x19\xae'
    
_lr_action_items = {'COMMENT':([0,1,3,4,5,6,10,11,12,13,15,16,18,19,22,23,24,25,26,28,29,30,31,32,35,36,44,45,46,47,48,49,],[10,-9,-16,-15,-11,-12,-18,-17,-10,-13,10,10,-8,-14,-22,-19,-20,-21,10,-24,-30,-29,-28,-25,-23,-26,-27,-35,-34,-33,-31,-32,]),'RBRACKET':([29,30,31,32,36,38,44,45,46,47,48,49,],[-30,-29,-28,-25,-26,44,-27,-35,-34,-33,-31,-32,]),'DIVIDE':([29,30,31,32,35,36,38,39,44,45,46,47,48,49,],[-30,-29,-28,-25,40,-26,40,40,-27,-35,-34,-33,40,40,]),'RPAREN':([29,30,31,32,36,39,44,45,46,47,48,49,],[-30,-29,-28,-25,-26,45,-27,-35,-34,-33,-31,-32,]),'GOTO':([0,1,3,4,5,6,10,11,12,13,15,16,18,19,22,23,24,25,26,28,29,30,31,32,35,36,44,45,46,47,48,49,],[9,-9,-16,-15,-11,-12,-18,-17,-10,-13,9,9,-8,-14,-22,-19,-20,-21,9,-24,-30,-29,-28,-25,-23,-26,-27,-35,-34,-33,-31,-32,]),'TIMES':([29,30,31,32,35,36,38,39,44,45,46,47,48,49,],[-30,-29,-28,-25,41,-26,41,41,-27,-35,-34,-33,41,41,]),'TEXT':([0,1,3,4,5,6,8,9,10,11,12,13,15,16,18,19,21,22,23,24,25,26,28,29,30,31,32,33,34,35,36,40,41,42,43,44,45,46,47,48,49,],[2,-9,-16,-15,-11,-12,23,24,-18,-17,-10,-13,2,2,-8,-14,31,-22,-19,-20,-21,2,-24,-30,-29,-28,-25,31,31,-23,-26,31,31,31,31,-27,-35,-34,-33,-31,-32,]),'NEWLINE':([0,1,3,4,5,6,10,11,12,13,15,16,18,19,22,23,24,25,26,28,29,30,31,32,35,36,44,45,46,47,48,49,],[11,-9,-16,-15,-11,-12,-18,-17,-10,-13,11,11,-8,-14,-22,-19,-20,-21,11,-24,-30,-29,-28,-25,-23,-26,-27,-35,-34,-33,-31,-32,]),'NUMBER':([0,1,4,5,6,9,10,11,12,13,16,18,19,21,22,23,24,25,26,28,29,30,31,32,33,34,35,36,40,41,42,43,44,45,46,47,48,49,],[3,-9,-15,-11,-12,25,-18,-17,-10,-13,3,-8,-14,30,-22,-19,-20,-21,3,-24,-30,-29,-28,-25,30,30,-23,-26,30,30,30,30,-27,-35,-34,-33,-31,-32,]),'LABEL':([0,1,3,4,5,6,10,11,12,13,15,16,18,19,22,23,24,25,26,28,29,30,31,32,35,36,44,45,46,47,48,49,],[4,-9,-16,-15,-11,-12,-18,-17,-10,-13,4,4,-8,-14,-22,-19,-20,-21,4,-24,-30,-29,-28,-25,-23,-26,-27,-35,-34,-33,-31,-32,]),'LBRACKET':([21,33,34,40,41,42,43,],[33,33,33,33,33,33,33,]),'PLUS':([29,30,31,32,35,36,38,39,44,45,46,47,48,49,],[-30,-29,-28,-25,42,-26,42,42,-27,-35,-34,-33,-31,-32,]),'LPAREN':([21,33,34,40,41,42,43,],[34,34,34,34,34,34,34,]),'PRINT':([0,1,3,4,5,6,10,11,12,13,15,16,18,19,22,23,24,25,26,28,29,30,31,32,35,36,44,45,46,47,48,49,],[7,-9,-16,-15,-11,-12,-18,-17,-10,-13,7,7,-8,-14,-22,-19,-20,-21,7,-24,-30,-29,-28,-25,-23,-26,-27,-35,-34,-33,-31,-32,]),'STRING':([7,21,],[22,28,]),'HEXNUM':([21,33,34,40,41,42,43,],[29,29,29,29,29,29,29,]),'MINUS':([29,30,31,32,35,36,38,39,44,45,46,47,48,49,],[-30,-29,-28,-25,43,-26,43,43,-27,-35,-34,-33,-31,-32,]),'ASSIGN':([2,],[21,]),'DEF':([0,1,3,4,5,6,10,11,12,13,15,16,18,19,22,23,24,25,26,28,29,30,31,32,35,36,44,45,46,47,48,49,],[8,-9,-16,-15,-11,-12,-18,-17,-10,-13,8,8,-8,-14,-22,-19,-20,-21,8,-24,-30,-29,-28,-25,-23,-26,-27,-35,-34,-33,-31,-32,]),'$end':([0,1,4,5,6,10,11,12,13,14,16,17,18,19,20,22,23,24,25,26,27,28,29,30,31,32,35,36,37,44,45,46,47,48,49,],[-7,-9,-15,-11,-12,-18,-17,-10,-13,0,-3,-2,-8,-14,-1,-22,-19,-20,-21,-4,-5,-24,-30,-29,-28,-25,-23,-26,-6,-27,-35,-34,-33,-31,-32,]),}

_lr_action = { }
for _k, _v in _lr_action_items.items():
   for _x,_y in zip(_v[0],_v[1]):
      if not _x in _lr_action:  _lr_action[_x] = { }
      _lr_action[_x][_k] = _y
del _lr_action_items

_lr_goto_items = {'comment':([0,15,16,26,],[1,1,1,1,]),'newline':([0,15,16,26,],[18,18,18,18,]),'number':([21,33,34,40,41,42,43,],[32,32,32,32,32,32,32,]),'label':([0,15,16,26,],[12,12,12,12,]),'assign_statement':([0,15,16,26,],[13,13,13,13,]),'program':([0,],[14,]),'lineno':([0,16,26,],[15,15,15,]),'statement':([0,15,16,26,],[16,26,16,16,]),'print_statement':([0,15,16,26,],[5,5,5,5,]),'def_statement':([0,15,16,26,],[19,19,19,19,]),'hexnum':([21,33,34,40,41,42,43,],[36,36,36,36,36,36,36,]),'expression':([21,33,34,40,41,42,43,],[35,38,39,46,47,48,49,]),'statement_list':([0,16,26,],[20,27,37,]),'empty':([0,16,26,],[17,17,17,]),'goto_statement':([0,15,16,26,],[6,6,6,6,]),}

_lr_goto = { }
for _k, _v in _lr_goto_items.items():
   for _x,_y in zip(_v[0],_v[1]):
       if not _x in _lr_goto: _lr_goto[_x] = { }
       _lr_goto[_x][_k] = _y
del _lr_goto_items
_lr_productions = [
  ("S' -> program","S'",1,None,None,None),
  ('program -> statement_list','program',1,'p_program','badlang_parser.py',105),
  ('statement_list -> empty','statement_list',1,'p_statement_list','badlang_parser.py',121),
  ('statement_list -> statement','statement_list',1,'p_statement_list','badlang_parser.py',122),
  ('statement_list -> lineno statement','statement_list',2,'p_statement_list','badlang_parser.py',123),
  ('statement_list -> statement statement_list','statement_list',2,'p_statement_list','badlang_parser.py',124),
  ('statement_list -> lineno statement statement_list','statement_list',3,'p_statement_list','badlang_parser.py',125),
  ('empty -> <empty>','empty',0,'p_empty','badlang_parser.py',130),
  ('statement -> newline','statement',1,'p_statement','badlang_parser.py',134),
  ('statement -> comment','statement',1,'p_statement','badlang_parser.py',135),
  ('statement -> label','statement',1,'p_statement','badlang_parser.py',136),
  ('statement -> print_statement','statement',1,'p_statement','badlang_parser.py',137),
  ('statement -> goto_statement','statement',1,'p_statement','badlang_parser.py',138),
  ('statement -> assign_statement','statement',1,'p_statement','badlang_parser.py',139),
  ('statement -> def_statement','statement',1,'p_statement','badlang_parser.py',140),
  ('label -> LABEL','label',1,'p_label','badlang_parser.py',146),
  ('lineno -> NUMBER','lineno',1,'p_lineno','badlang_parser.py',151),
  ('newline -> NEWLINE','newline',1,'p_newline','badlang_parser.py',157),
  ('comment -> COMMENT','comment',1,'p_comment','badlang_parser.py',170),
  ('def_statement -> DEF TEXT','def_statement',2,'p_def_statement','badlang_parser.py',177),
  ('goto_statement -> GOTO TEXT','goto_statement',2,'p_goto_statement','badlang_parser.py',186),
  ('goto_statement -> GOTO NUMBER','goto_statement',2,'p_goto_statement','badlang_parser.py',187),
  ('print_statement -> PRINT STRING','print_statement',2,'p_print_statement','badlang_parser.py',195),
  ('assign_statement -> TEXT ASSIGN expression','assign_statement',3,'p_assign_statement','badlang_parser.py',202),
  ('assign_statement -> TEXT ASSIGN STRING','assign_statement',3,'p_assign_statement','badlang_parser.py',203),
  ('expression -> number','expression',1,'p_expression_singleop','badlang_parser.py',216),
  ('expression -> hexnum','expression',1,'p_expression_singleop','badlang_parser.py',217),
  ('expression -> LBRACKET expression RBRACKET','expression',3,'p_expression_singleop','badlang_parser.py',218),
  ('expression -> TEXT','expression',1,'p_expression_text','badlang_parser.py',227),
  ('number -> NUMBER','number',1,'p_number','badlang_parser.py',231),
  ('hexnum -> HEXNUM','hexnum',1,'p_hexnum','badlang_parser.py',237),
  ('expression -> expression PLUS expression','expression',3,'p_expression_binop','badlang_parser.py',243),
  ('expression -> expression MINUS expression','expression',3,'p_expression_binop','badlang_parser.py',244),
  ('expression -> expression TIMES expression','expression',3,'p_expression_binop','badlang_parser.py',245),
  ('expression -> expression DIVIDE expression','expression',3,'p_expression_binop','badlang_parser.py',246),
  ('expression -> LPAREN expression RPAREN','expression',3,'p_expression_group','badlang_parser.py',265),
]
