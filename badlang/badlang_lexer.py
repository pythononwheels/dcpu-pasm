# -----------------------------------------------------------------------------
# dcpu 16 asm lexer
# khz 2013
# khz@tzi.org
# www.pythononwheels.org
# -----------------------------------------------------------------------------
import sys
import ply.lex as lex
import string


commands = {
    # Format
    # command    (token.type, OPCODE_BLOCK)
    # default is OPCODE_BLOCK = op_OPCODE (e.g.: REM => op_REM)  
    "print" :   ("PRINT","op_PRINT"),
    "goto"  :   ("GOTO","op_GOTO"),
    "for"   :   ("FOR","op_FOR"),
    "next"  :   ("NEXT", "op_NEXT"),
    "if"    :   ("IF","op_IF"),
    "then"  :   ("THEN","op_THEN"),
    "else"  :   ("ELSE","op_ELSE"), 
    "def"   :   ("DEF", "op_DEF")
}

# Tokens
t_LPAREN    = r'\('
t_RPAREN    = r'\)'
t_INTEGER   = r'\d+'    
t_LABEL     = r':[a-zA-Z_][a-zA-Z0-9_]*'
t_TEXT      = r'[w]+'
t_RBRACKET  = r']'
t_LBRACKET  = r'\['
t_COMMA     = r","
t_QUOTES    = r"[\"\']"
t_PLUS      = r"\+"
t_MINUS     = r"-"
t_TIMES     = r"\*"
t_DIVIDE    = r"/"
t_LESS      = r"<"
t_MORE      = r">"
t_EQUALS     = r"=="
t_ASSIGN     = r"="

tokens =  [
   "LPAREN","RPAREN", "INTEGER", "LABEL", "RBRACKET", "LBRACKET",
   "COMMA", "QUOTES", "PLUS", "MINUS", "TIMES", "DIVIDE", "LESS", "MORE", "EQUALS", "ASSIGN",
   "HEXNUM","NUMBER", "COMMENT", "NEWLINE", "STRING", "COMMAND", "TEXT"
] 

for k in commands.keys():
    tokens.append(commands[k][0])

#print tokens

# Ignored characters
t_ignore = " \t"

def t_STRING(t):
    # *? means => non-greedy * (see:http://docs.python.org/2/library/re.html )
    r"\"[^']*?\""
    t.value = t.value[1:-1]
    return t

def t_HEXNUM(t):
    r'0x[0-9a-fA-F]+'
    #print t.value
    #print t.value[2:len(t.value)]
    t.value = int(t.value[2:len(t.value)],16)
    return t
       
def t_NUMBER(t):
    r'\d+'
    try:
        t.value = int(t.value)
    except ValueError:
        print("Integer value too large %d", t.value)
        t.value = 0
    return t

def t_COMMENT(t):
    r'\#.*'
    # No return value. Token discarded
    t.type = "COMMENT"
    return t

def t_NEWLINE(t):
    r'[\n\r\n]+'
    t.lexer.lineno += t.value.count("\n")
    return t

def t_COMMAND(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    #print "LEXER: I have this: ", t.value, t.type
    lower_t_value = string.lower(t.value)
    # check for pseude_op dat 
    if lower_t_value == "dat":
      t.type = "DAT"
      return t
    t.type = commands.get(lower_t_value)    # Check for reserved words
    if t.type:
        t.type = t.type[0]
        #print "LEXER: special_opcodes: ", t.value, t.type
        return t
    else:
        t.type = "TEXT"
        #print "LEXER text: ", t.value, t.type
      
    return t
    
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)
    

if __name__ == "__main__":
    lexer = lex.lex()
    print tokens