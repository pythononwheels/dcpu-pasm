#
#
#
# dlang to asm
#
import os
from badlang_parser import Node
tab = "\t"


class DlangToAsm():
    def __init__(self, ast, data, symbol_table):
        DlangToAsm.ast = ast
        DlangToAsm.data = data
        DlangToAsm.symbol_table = symbol_table

    # 
    # asm generation methods
    #
    def op_COMMENT(self, ast_entry):
        """ A COMMENT 
            ; <comment>
        """
        code = "; " + str(ast_entry[1]) + os.linesep
        return code

    def op_DEF(self, ast_entry):
        """ DEF Statement 
            :lable_<symbol_name>_<symbol_scope>
                DAT 0x0000
        """
        code = ":label" + str(ast_entry[1]) + str(ast_entry[2]) + os.linesep
        code += tab + "DAT 0x0000"  
        return code
    
    def op_ASSIGN(self, ast_entry):
        """ Assignement statement 
            set lable_<symbol_name>_<symbol_scope>, value
        """
        code = ""
        op1 = ast_entry[1]
        op2 = ast_entry[2]
        if isinstance(op1,str):
            if isinstance(op2, Node):
                code = "set label_" + op1[0] + "_" + op1[1] + ", " + op2
        else:
            err_msg = "No expressions as left hand sides. Statement: %s"  % (ast_entry)
            raise Exception(err_msg)
        
        return code



    #
    # helper methods
    #
    def split_hi_lo(self, word):
        """ 
            Splits a word into it's high and low byte
        """
        hi = (word & 0xFF00) >> 8
        lo = (word & 0x00FF)        
        return hi,lo