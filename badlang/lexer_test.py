# Build the lexer
import ply.lex as lex
import badlang_lexer
import sys

if __name__ == "__main__":
    
    if len(sys.argv) < 2:
        print "usage from cli: python lexer_test.py <input_file.dlg>"
        sys.exit()
    elif sys.argv[1] in ["-h", "--help", "/?"]:
        print "usage from cli: python lexer_test.py <input_file.dlg>"
        sys.exit()
    
    #for elem in special_opcodes:
    #    print "| %s" % special_opcodes[elem]
    mylexer = lex.lex( module=badlang_lexer)
    filename = sys.argv[1]

    f = open(filename, "r")
    
    print "-- Scanning file: %s" % (filename)
    sometext = f.read()
    mylexer.input(sometext)
    while 1:
        tok = mylexer.token()
        if not tok: break
        #tok has the following attributes:
        # 'lexpos', 'lineno', 'type', 'value'
        #if tok.type not in  ["NEWLINE", "COMMA", "TEXT", "RBRACKET", "LBRACKET"]:
        print tok.value, "\t\t", tok
        
    print "----------------------> Done"