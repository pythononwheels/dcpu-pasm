# -*- coding: utf-8 -*-
#
# CPU
# see: http://0x10cwiki.com/wiki/DCPU-16
# for opcodes see: http://0x10cwiki.com/wiki/Instruction_set 
#
from optparse import OptionParser
import sys, os
import array
import struct
import debugmon
# BIT Masks for opcode detection. 
# see: http://0x10cwiki.com/wiki/Instruction_set
#       section : Machine Code Format
# For example, SET is a basic instruction with the format
#   aaaaaa bbbbb 00001
# However, JSR is a special instruction with the format
#   aaaaaa 00001 00000

MASK_A = 0b1111110000000000
MASK_B = 0b0000001111100000
MASK_O = 0b0000000000011111
RAM_START = 0x0040

opcodes = {
    # format: 0xOPCODE  : ( function to call, dcpu-16 cycles )
    # remark: a + in the comment means that the cycles are 2+ (for the INTs example)
    # see: http://www.the0x10cwiki.net/DCPU-16/de
    0x00    :   ("op_SPECIAL", 0),
    0x01    :   ("op_SET",1),      
    0x02    :   ("op_ADD",2),      
    0x03    :   ("op_SUB",2),      
    0x04    :   ("op_MUL",2),      
    0x05    :   ("op_MLI",2),      
    0x06    :   ("op_DIV",3),      
    0x07    :   ("op_DVI",3),
    0x08    :   ("op_MOD",3),
    0x09    :   ("op_MDI",3),
    0x0a    :   ("op_AND",1),
    0x0b    :   ("op_BOR",1),
    0x0c    :   ("op_XOR",1),
    0x0d    :   ("op_SHR",1),
    0x0e    :   ("op_ASR",1),
    0x0f    :   ("op_SHL",1),
    0x10    :   ("op_IFB",2), #+
    0x11    :   ("op_IFC",2), #+
    0x12    :   ("op_IFE",2), #+
    0x13    :   ("op_IFN",2), #+
    0x14    :   ("op_IFG",2), #+
    0x15    :   ("op_IFA",2), #+
    0x16    :   ("op_IFL",2), #+
    0x17    :   ("op_IFU",2), #+
    0x18    :   ("op_NA",0),
    0x19    :   ("op_NA",0),
    0x1a    :   ("op_ADX",3),
    0x1b    :   ("op_SBX",3),
    0x1c    :   ("op_NA",0),
    0x1d    :   ("op_NA",0),
    0x1e    :   ("op_STI",2),
    0x1f    :   ("op_STD",2) 
}    

special_opcodes = {
    0x00    :   ("op_RESERVED", 0),
    0x01    :   ("op_JSR",1),      
    0x02    :   ("op_NA",0),      
    0x03    :   ("op_NA",0),      
    0x04    :   ("op_NA",0),      
    0x05    :   ("op_NA",0),      
    0x06    :   ("op_NA",0),      
    0x07    :   ("op_NA",0),      
    0x08    :   ("op_INT",4),
    0x09    :   ("op_IAG",1),
    0x0a    :   ("op_IAS",1),
    0x0b    :   ("op_RFI",3),
    0x0c    :   ("op_IAQ",2),
    0x0d    :   ("op_NA",0),
    0x0e    :   ("op_NA",0),
    0x0f    :   ("op_NA",0),
    0x10    :   ("op_HWN",2), 
    0x11    :   ("op_HWQ",4),
    0x12    :   ("op_HWI",4), #+
    0x13    :   ("op_NA",0),
    0x14    :   ("op_NA",0),
    0x15    :   ("op_NA",0),
    0x16    :   ("op_NA",0),
    0x17    :   ("op_NA",0),
    0x18    :   ("op_NA",0),
    0x19    :   ("op_NA",0),
    0x1a    :   ("op_NA",0),
    0x1b    :   ("op_NA",0),
    0x1c    :   ("op_NA",0),
    0x1d    :   ("op_NA",0),
    0x1e    :   ("op_NA",0),
    0x1f    :   ("op_NA",0)
}    

class Cpu(object):
    """ represents the good old DCPU-16 """
    
    def __init__(self):
        # general purpose registers (16-BIT)
        self.A = 0b0000000000000000
        self.B = 0b0000000000000000
        self.C = 0b0000000000000000
        self.D = 0b0000000000000000
        self.Y = 0b0000000000000000
        self.Z = 0b0000000000000000
        self.I = 0b0000000000000000
        self.J = 0b0000000000000000
        # special registers (16-BIT)
        self.PC = 0b0000000000000000
        self.SP = 0b0000000000000000
        self.EX = 0b0000000000000000
        self.IA = 0b0000000000000000
        # the number of elapsed cycles
        self.ticks = 0
        
        self.ram = []
        self.ramsize = 0x10000
        print self.ramsize
        # ram = 65536 * 16 BIT
        for i in range(0,self.ramsize-1):
            self.ram.append(0x0000)
            
        
            
    def _exec( self,mem_start, mem_end=0 ):
        """ scan and execute a word sequence 
            remark from the DCPU-16 documentation: 
                    (see: http://dcpu.com/dcpu-16/)
            quote:  Instructions are 1-3 words long and are 
                    fully defined by the first word.            
        """
        opcode = self.ram[hex(self.PC)] & MASK_O
    
    def op_SET(self):
        """
            The Set Operation
        """
        print "op_SET called"

    def split_hi_lo(self, word):
        """ 
            Splits a word into it's high and low byte
        """
        hi = (word & 0xFF00) >> 8
        lo = (word & 0x00FF)        
        return hi,lo
                        
    def dump_ram_per_line(self, start=RAM_START, end=RAM_START+500):
        """ 
            a linewise dump of the RAM to the console
        """
        for counter in range(start,end):
            hi,lo = self.split_hi_lo(self.ram[counter])
            print "{0:04X} {1:04X}  {1:016b}".format(counter, self.ram[counter]),
            try:
                print unichr(hi),
                print unichr(lo)
            except:
                print "."
            
    def dump_ram(self, start=RAM_START, end=RAM_START+500):
        """
            Dump RAM to console in a format like HEX Monitors: (console paste below)
            
            0070: 00 00 7C 01 BE EF 01 E1 10 00 78 0D 10 00 . . | . � � . � . . x . . .
            0080: 80 61 81 6C 00 13 7D C1 00 20 59 61 80 00 . a . l . . } � . . Y a . .
            0090: 7D C1 00 09 00 48 00 65 00 6C 00 6C 00 6F } � . . . H . e . l . l . o
            00A0: 00 6F 00 72 00 6C 00 64 00 21 00 00 85 C3 . o . r . l . d . ! . . . �
            00B0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 . . . . . . . . . . . . . .
            00C0: 00 01 00 02 00 03 00 04 00 05 00 06 00 07 . . . . . . . . . . . . . .
        """
        chars = []
        print "start = ", start
        print "end = ", end
        times = (end-start)/8
        print "runs %s times"  % times
        for counter in range(0,times):
            #print "counter:",counter
            print "{0:04X}:".format(counter*8),
            for i in range(counter*8,counter*8+8):
                #print "counter+i:", counter+i
                hi,lo = self.split_hi_lo(self.ram[counter+i])
                print "{0:02X}{1:02X}".format(hi,lo),
                chars.append(hi)
                chars.append(lo)
                
            for char in chars:
                self.printchar(char)
                    
            chars = []
            print
            
    def printchar(self,byte):
        """
            print bytes as chars on the console.
            replace unprintable and special chars by a . (dot)
        """
        if byte > 0x20:
            try:
                print unichr(byte),
            except:
                print ".",
        else:
            print ".",
    def get_ram(self, start=RAM_START, end=RAM_START+500):
        """
            return the ram as a formatted list of strings.
        """
        olist = []
        for counter in range(start,end):
            ostr = ""
            hi,lo = self.split_hi_lo(self.ram[counter])
            ostr +=  "{0:04X} {1:04X}  {1:016b}".format(counter, self.ram[counter])
            try:
                ostr += unichr(hi) + " "
                ostr += unichr(lo) + os.linesep
            except:
                ostr += "." + os.linesep
            olist.append(ostr)
        return olist
    
if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option( "-f", "--file", action="store", type="string", 
                       dest="objfile", help="file with assembled DCPU-16 oject code", 
                       default ="None" )
    
    (options, args) = parser.parse_args()
    print options
    
    ###########################################
    # test to call the according function from
    # the opcode map.
    # example for 0x001 = SET
    #############################################
    cpu = Cpu()
    for key in opcodes:
        #print opcodes[key]
        if key == 0x01:
            print "calling: %s" %(opcodes[key][0])
            getattr(cpu, opcodes[key][0])()
    
    if options.objfile == "None":
        print 
        print "INFO: you have to give an input file using -f in.bin16"
        print "INFO: try python cpu.py -f hello.bin"
        sys.exit(0)
        
    print "##########################################"
    print "# DCPU-16 Test                           #"
    print "##########################################"
    infile = open( options.objfile, "rb")
    counter = RAM_START
    while True:
        word = infile.read(2)
        if not word:
            break
        # see: http://docs.python.org/2/library/struct.html
        # > == Big Endian, H == unsigned short
        word = struct.unpack(">H", word )[0]
        cpu.ram[counter] = word
        counter += 1
    
    
    #for i in range(0,128):
    #    cpu.ram[counter+10+i] = i
        
    
    cpu.dump_ram()
    
    #print cpu.dump_ram()
    ram = cpu.get_ram()
    #print ram
    win = debugmon.DebugWin(cpu)
    
    
           
    
    
    
    