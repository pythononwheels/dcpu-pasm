# -----------------------------------------------------------------------------
# dcpu 16 asm lexer
# khz 2013
# khz@tzi.org
# www.pythononwheels.org
# -----------------------------------------------------------------------------
import sys
import ply.lex as lex
import string

enhanced_opcodes = {
    
}
opcodes = {
    # Format
    # opcode    (token.type, 0xOPCODE,dcpu-16 cycles, function to call)
    #"ose"  :   ("SPECIAL", 0x00,0, "op_SPECIAL")
    "set"   :   ("SET",0x01,1, "op_SET"),
    "add"   :   ("ADD",0x02,2, "op_ADD"),
    "sub"   :   ("SUB",0x03,2, "op_SUB"),
    "mul"   :   ("MUL",0x04,2, "op_MUL"), 
    "mli"   :   ("MLI",0x05,2, "op_MLI"),
    "div"   :   ("DIV",0x06,3, "op_DIV"),
    "dvi"   :   ("DVI",0x07,3, "op_DVI"),
    "mod"   :   ("MOD",0x08,3, "op_MOD"),
    "mdi"   :   ("MDI",0x09,3, "op_MDI"),
    "and"   :   ("AND",0x0a,1, "op_AND"),
    "bor"   :   ("BOR",0x0b,1, "op_BOR"), 
    "xor"   :   ("XOR",0x0c,1, "op_XOR"), 
    "shr"   :   ("SHR",0x0d,1, "op_SHR"), 
    "asr"   :   ("ASR",0x0e,1, "op_ASR"), 
    "shl"   :   ("SHL",0x0f,1, "op_SHL"), 
    "ifb"   :   ("IFB",0x10,2, "op_IFB"), #+
    "ifc"   :   ("IFC",0x11,2, "op_IFC"), #+
    "ife"   :   ("IFE",0x12,2, "op_IFE"), #+
    "ifn"   :   ("IFN",0x13,2, "op_IFN"), #+
    "ifg"   :   ("IFG",0x14,2, "op_IFG"), #+
    "ifa"   :   ("IFA",0x15,2, "op_IFA"), #+
    "ifl"   :   ("IFL",0x16,2, "op_IFL"), #+
    "ifu"   :   ("IFU",0x17,2, "op_IFU"), #+
   #"nop"   :   ("NOP",0x18,0, "op_NOP"),
   #"nop"   :   ("NOP",0x18,0, "op_NOP"),
    "adx"   :   ("ADX",0x1a,3, "op_ADX"), 
    "sbx"   :   ("SBX",0x1b,3, "op_SBX"),
    "sti"   :   ("STI",0x1e,2, "op_STI"),
    "std"   :   ("STD",0x1f,2, "op_STD")
}
special_opcodes = {
   #"res"   :   ("RESERVED", 0x00,0, "op_RESERVED"),
    "jsr"   :   ("JSR",0x01,1, "op_JSR"),
   #"nop"   :   ("NOP",0x02,0, "op_NOP"),
   #"nop"   :   ("NOP",0x03,0, "op_NOP"),
   #"nop"   :   ("NOP",0x04,0, "op_NOP"),
   #"nop"   :   ("NOP",0x05,0, "op_NOP"),
   #"nop"   :   ("NOP",0x06,0, "op_NOP"),
   #"nop"   :   ("NOP",0x07,0, "op_NOP"),
    "int"   :   ("INT",0x08,4, "op_INT"),
    "iag"   :   ("IAG",0x09,1, "op_IAG"),
    "ias"   :   ("IAS",0x0a,1, "op_IAS"),
    "rfi"   :   ("RFI",0x0b,3, "op_RFI"),
    "iaq"   :   ("IAQ",0x0c,2, "op_IAQ"),
   #"nop"   :   ("NOP",0x0d,0, "op_NOP"),
   #"nop"   :   ("NOP",0x0e,0, "op_NOP"),
   #"nop"   :   ("NOP",0x0f,0, "op_NOP"),
    "hwn"   :   ("HWN",0x10,2, "op_HWN"),
    "hwq"   :   ("HWQ",0x11,4, "op_HWQ"),
    "hwi"   :   ("HWI",0x12,4, "op_HWI"), #+
   #"nop"   :   ("NOP",0x13,0, "op_NOP"), 
   #"nop"   :   ("NOP",0x14,0, "op_NOP"), 
   #"nop"   :   ("NOP",0x15,0, "op_NOP"), 
   #"nop"   :   ("NOP",0x16,0, "op_NOP"), 
   #"nop"   :   ("NOP",0x17,0, "op_NOP"), 
   #"nop"   :   ("NOP",0x18,0, "op_NOP"), 
   #"nop"   :   ("NOP",0x19,0, "op_NOP"), 
   #"nop"   :   ("NOP",0x1a,0, "op_NOP"), 
   #"nop"   :   ("NOP",0x1b,0, "op_NOP"), 
   #"nop"   :   ("NOP",0x1c,0, "op_NOP"), 
   #"nop"   :   ("NOP",0x1d,0, "op_NOP"), 
   #"nop"   :   ("NOP",0x1e,0, "op_NOP"), 
   #"nop"   :   ("NOP",0x1f,0, "op_NOP") 
}

register = {
    "a"    :   "A",
    "b"    :   "B",
    "c"    :   "C",
    "d"    :   "D",
    "y"    :   "Y",
    "z"    :   "Z",
    "i"    :   "I",
    "j"    :   "J",
    "pc"    :   "PC",
    "sp"    :   "SP",
    "ex"    :   "EX",
    "ia"    :   "IA"
}

# Tokens
t_LPAREN    = r'\('
t_RPAREN    = r'\)'
t_INTEGER   = r'\d+'    
t_LABEL     = r':[a-zA-Z_][a-zA-Z0-9_]*'
t_RBRACKET  = r']'
t_LBRACKET  = r'\['
t_COMMA     = r","
t_QUOTES    = r"[\"\']"
t_PLUS      = r"\+"
t_MINUS     = r"-"
t_TIMES     = r"\*"
t_DIVIDE    = r"/"

tokens =  [
   "LPAREN","RPAREN", "INTEGER", "LABEL", "NAME", "RBRACKET", "LBRACKET",
   "COMMA", "QUOTES", "TEXT", "PLUS", "MINUS", "TIMES", "DIVIDE",
   "HEXNUM","NUMBER", "COMMENT", "NEWLINE", "STRING", "COMMAND", "REGISTER", "DAT"
] + list(register.values()) 

for k in opcodes.keys():
    tokens.append(opcodes[k][0])

for k in special_opcodes.keys():
    tokens.append(special_opcodes[k][0])

print tokens

# Ignored characters
t_ignore = " \t"

def t_STRING(t):
    # *? means => non-greedy * (see:http://docs.python.org/2/library/re.html )
    r'\'[^"]*?\''
    t.value = t.value[1:-1]
    return t

def t_HEXNUM(t):
    r'0x[0-9a-fA-F]+'
    #print t.value
    #print t.value[2:len(t.value)]
    t.value = int(t.value[2:len(t.value)],16)
    return t
       
def t_NUMBER(t):
    r'\d+'
    try:
        t.value = int(t.value)
    except ValueError:
        print("Integer value too large %d", t.value)
        t.value = 0
    return t

def t_COMMENT(t):
    r'\;.*'
    # No return value. Token discarded
    pass

def t_NEWLINE(t):
    r'[\n\r\n]+'
    t.lexer.lineno += t.value.count("\n")
    return t

def t_COMMAND(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    #print "LEXER: I have this: ", t.value, t.type
    lower_t_value = string.lower(t.value)
    # check if dat keyword
    if lower_t_value == "dat":
      t.type = "DAT"
      return t
    t.type = register.get(lower_t_value)    # Check for reserved words
    if t.type:
        #print "LEXER opcode: ", t.value, t.type
        t.type = "REGISTER"
        return t
    t.type = opcodes.get(lower_t_value)    # Check for reserved words
    if t.type:
        #print "LEXER opcode: ", t.value, t.type
        t.type = t.type[0]
        return t
    t.type = special_opcodes.get(lower_t_value)     # Check for reserved words
    if t.type:
        #print "LEXER: special_opcodes: ", t.value, t.type
        return t
    else:
        t.type = "TEXT"
        #print "LEXER text: ", t.value, t.type
      
    return t
    
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)
    


lexer = lex.lex()