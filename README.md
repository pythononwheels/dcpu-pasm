dcpu-pasm
===================
This repo conmtains two things:

* 	**pasm** => a python/PLY based assembler for dcpu-16
* 	**BADLang** => a 'higher'-level (basic/python syntax alike) programming language for the dcpu-16

Why (and why you should help)
-------------------------
I was always very interested in interpreter and compiler design
but never really got to the point of implementing one myself.
The DCPU-16 with it's simple architecture and approach combined with
some books about ANTLR I was reading kicked me of to
really do it this time.

And by now I am already pretty happy with the result.
It is not done by far but pasm lexes and parses notch's test.asm fine and
produces an AST.

Next steps should be easy with YOUR help! ... come on ... ;)

From my own experience I can just say that
**I swear that:**
	
>   "if you ever wanted to dive into compiler or interpreter construction
	this is the right time and the right place.
	If you know python than PLY is a very good base for you.
	It uses the same paradigms as LEX/YACC which are THE standard tools for 
	lexer and parser generators around.

>	If you ever bought a book on compiler constuction and 
	were stuck at the calculator example and wondered how to 
	get ahead from there but didn't get a clue. Well, here you are. ;)
	Just look at asm_parser.py and you'll see.
	

Pasm: 
------------------
* Uses PLY as the foundation
** Makes the Assembler easily extensible, fast an reliable.


BADLang (Beginners All Purpose Dcpu Language)
----------------
Since the work on pasm went so well and I had a good foundation in the
assembler I thought it would be nice to implement a 'higher'-level language.
So I made a first shot of a small language that is sytactically a little
aligned to BASIC and python.

The BADLang "compiler" (shall) produce dcpu-16 assembler in the end. 
Which then can be compiled by pasm or any other dcpu-16 assembler into
object code.

So BADLang will make it easier to write dcpu-16 code. Even BADLang can
be extended in writing BADLang if some basic operations are implemented.

BADLang example:
------------------------    
**(just to get an idea - and it's really just my first shot)**
	
	#
	# A First BADLang Program for the DCPU-16
	# khz July / 2013
	#

	def b
	x = 0
	:label1
		print "Hello World"
		x = x + 2 * 3 + b
	goto label1
	a = ( b * ( 2 *4 ) )
	#for x < 10:
	#  print "test"
	#  x = x + 1 
	#next  


-- > I know that there is a *goto* but this is dcpu . that's the way aha aha I like it aha aha ;)
-- > No, honestly there is an instruction like this in dcpu-16 assembler:

	:label1
		IFE [0x9000+I],0
 		SET PC,label1

And that's pretty much the *goto* above in BADLang (we dont need that in the end, but might keep it because we want it)


Parser output:
------------------------
		BADLang program found

	 -> Final Symbol table:
	symbol_table:
	----------------------------------------
	          scope         Symbol Name
	----------------------------------------
	         global                   b
	         global                   x
	         global                   a

	 -> Final Data section:
	data
	----------------------------------------
	           ['Hello World']
	----------------------------------------
	 -> final AST:
	abstract syntax tree:
	----------------------------------------
	['COMMENT', '#', None]
	['COMMENT', '# A First BADLang Program for the DCPU-16', None]
	['COMMENT', '# khz July / 2013', None]
	['COMMENT', '#', None]
	['DEF', 'b', 'global']
	['ASSIGN',
	   |-> label(:,x), number(#,0)]
	['LABEL', ':label1', None]
	['PRINT', 'Hello World', None]
	['ASSIGN',
	   |-> label(:,x),
	     |-> binop(+,
	       |-> binop(+,
	         |-> label(:,x),
	           |-> binop(*,number(#,2),number(#,3))),
	             |-> label(:,b))]
	['GOTO', 'label1', None]
	['ASSIGN',
	   |-> label(:,a), group((),
	     |-> binop(*,
	       |-> label(:,b),group((),
	         |-> binop(*,number(#,2),number(#,4)))))]
	['COMMENT', '#for x < 10:', None]
	['COMMENT', '#  print "test"', None]
	['COMMENT', '#  x = x + 1 ', None]
	['COMMENT', '#next', None]

Project Status (as of 4.7.2013)
=====================
pasm
--------
- pasm lexer is fully implemented
- pasm compiler parses notch's test.asm fine and produces an AST
- Missing: object code generation (next step)
- Missing: implement all opcodes (step-by-step) 

BADLang:
------------
- Name is BAD but could be worse... like WORSELang ;)
- lexes and parses fine (the testfile) see above.
- Missing: needs more statements 
- Missing: dcpu-16 asm generation

Usage:
=======
pasm:
-----
to test the lexer only: 

	python test_lexer.py test.asm 

to test the parser: 

	python asm_parser.py test.asm

BADLang
----------
to test the lexer only: 

	python lexer_test.py test_badlang.blg

to test the parser: 

	python badlang_parser.py test_badlang.blg

Contributors very welcome
============================

PLease feel free to fork or contribute to this 

Just give it a try. Any comments are very welcome. 
Mail to khz@pythononwheels.org

**Read more on [the blog](http://www.pythononwheels.org/post/blog)**

Kind regards,
klaas