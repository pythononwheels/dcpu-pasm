#
# debugging monitor
# showing memory and cpu info
# of the dcpu-16

import Tkinter
import ttk
import os

class DebugWin(Tkinter.Frame):

    def __init__(self, cpu):
        root = Tkinter.Tk()
        root.title("khz's little dcpu-16 debuggin monitor (c) 1986")
        Tkinter.Frame.__init__(self, root)
        self.grid()
        self.create_widgets()
        self.cpu = cpu
        self.set_text(0)
        self.set_text2(0)
        root.mainloop()

    def create_widgets(self):
      
        
        self.memlabel = Tkinter.Label(self, text="Insert start MEM Addr:")
        self.memlabel.grid(row=1, column=0, columnspan=2, sticky=Tkinter.W)
        
        self.mem_start= Tkinter.Entry(self)
        self.mem_start.grid(row=1, column=1, sticky=Tkinter.W)
        
        self.refresh_button = Tkinter.Button(self, text="submit", command=self.reload)
        self.refresh_button.grid(row=1, column=2, sticky=Tkinter.W)
        
        # first textbox
        self.text = Tkinter.Text(self, width=70, height=30, wrap=Tkinter.NONE)
        self.text.grid(row=3, column=0,columnspan=2,sticky=Tkinter.W)
        
        self.scroller = Tkinter.Scrollbar(self)
        self.scroller.grid(row=3,column=3,sticky=Tkinter.W)
        self.scroller.config(command=self.text.yview)
        self.text.config(yscrollcommand=self.scroller.set)
        
        # the second textbox
        self.text2 = Tkinter.Text(self, width=70, height=30, wrap=Tkinter.NONE)
        self.text2.grid(row=3, column=4,columnspan=2,sticky=Tkinter.W)
        
        self.scroller2 = Tkinter.Scrollbar(self)
        self.scroller2.grid(row=3,column=7,sticky=Tkinter.W)
        self.scroller2.config(command=self.text2.yview)
        self.text2.config(yscrollcommand=self.scroller2.set)
    
    def set_text(self, start):
        i = 1 
        self.text.delete(1.0, Tkinter.END)
        for line in self.cpu.get_ram(start, start+500):
            #print line
            self.text.insert("%s.0" % (i), line)
            i += 1
            
    
    def set_text2(self, start):
        self.text2.delete(1.0, Tkinter.END)
        chars = []
        end = start+500
        times = (end-start)/8
        ostr = ""
        
        for counter in range(start,times):
            #print "counter:",counter
            ostr = "{0:04X}:".format((counter*8))
            for i in range(counter*8,counter*8+8):
                #print "counter+i:", counter+i
                hi,lo = self.cpu.split_hi_lo(self.cpu.ram[counter+start+i])
                ostr += " {0:02X}{1:02X} ".format(hi,lo)
                chars.append(hi)
                chars.append(lo)
                
            for char in chars:
                if char > 0x20 and char <128:
                    try:
                        ostr += unichr(char)
                    except:
                        ostr += "." 
                else:
                    ostr +=  "."
            ostr += os.linesep
            #print counter, ostr
            self.text2.insert("%s.0" % (counter+1), ostr)        
            ostr = ""
            chars = []
            
    def reload(self):
        start = int(self.mem_start.get())
        print "start:", start
        self.set_text(start)
        self.set_text2(start)
