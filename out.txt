-- Scanning file: test.asm

		LexToken(NEWLINE,'\n',1,25)


		LexToken(NEWLINE,'\n\n',2,45)
:start 		LexToken(LABEL,':start',4,47)

		LexToken(NEWLINE,'\n',4,53)
set 		LexToken(SET,'set',5,55)
i 		LexToken(REGISTER,'i',5,59)
, 		LexToken(COMMA,',',5,60)
0 		LexToken(NUMBER,0,5,62)

		LexToken(NEWLINE,'\n',5,63)
set 		LexToken(SET,'set',6,65)
j 		LexToken(REGISTER,'j',6,69)
, 		LexToken(COMMA,',',6,70)
0 		LexToken(NUMBER,0,6,72)

		LexToken(NEWLINE,'\n',6,73)
set 		LexToken(SET,'set',7,75)
b 		LexToken(REGISTER,'b',7,79)
, 		LexToken(COMMA,',',7,80)
0xf100 		LexToken(HEXNUM,'0xf100',7,82)


		LexToken(NEWLINE,'\n\n',7,88)
:nextchar 		LexToken(LABEL,':nextchar',9,90)

		LexToken(NEWLINE,'\n',9,99)
set 		LexToken(SET,'set',10,101)
a 		LexToken(REGISTER,'a',10,105)
, 		LexToken(COMMA,',',10,106)
[ 		LexToken(LBRACKET,'[',10,108)
data 		LexToken(TEXT,'data',10,109)
+ 		LexToken(PLUS,'+',10,113)
i 		LexToken(REGISTER,'i',10,114)
] 		LexToken(RBRACKET,']',10,115)

		LexToken(NEWLINE,'\n',10,116)
ife 		LexToken(IFE,'ife',11,118)
a 		LexToken(REGISTER,'a',11,122)
, 		LexToken(COMMA,',',11,123)
0 		LexToken(NUMBER,0,11,125)

		LexToken(NEWLINE,'\n',11,126)
set 		LexToken(SET,'set',12,132)
PC 		LexToken(REGISTER,'PC',12,136)
, 		LexToken(COMMA,',',12,138)
end 		LexToken(TEXT,'end',12,140)

		LexToken(NEWLINE,'\n',12,143)
ifg 		LexToken(IFG,'ifg',13,145)
a 		LexToken(REGISTER,'a',13,149)
, 		LexToken(COMMA,',',13,150)
0xff 		LexToken(HEXNUM,'0xff',13,152)

		LexToken(NEWLINE,'\n',13,156)
set 		LexToken(SET,'set',14,162)
PC 		LexToken(REGISTER,'PC',14,166)
, 		LexToken(COMMA,',',14,168)
setcolor 		LexToken(TEXT,'setcolor',14,170)

		LexToken(NEWLINE,'\n',14,178)
bor 		LexToken(BOR,'bor',15,180)
a 		LexToken(REGISTER,'a',15,184)
, 		LexToken(COMMA,',',15,185)
b 		LexToken(REGISTER,'b',15,187)

		LexToken(NEWLINE,'\n',15,188)
set 		LexToken(SET,'set',16,190)
[ 		LexToken(LBRACKET,'[',16,194)
0x8000 		LexToken(HEXNUM,'0x8000',16,195)
+ 		LexToken(PLUS,'+',16,201)
j 		LexToken(REGISTER,'j',16,202)
] 		LexToken(RBRACKET,']',16,203)
, 		LexToken(COMMA,',',16,204)
a 		LexToken(REGISTER,'a',16,206)

		LexToken(NEWLINE,'\n',16,207)
add 		LexToken(ADD,'add',17,209)
i 		LexToken(REGISTER,'i',17,213)
, 		LexToken(COMMA,',',17,214)
1 		LexToken(NUMBER,1,17,216)

		LexToken(NEWLINE,'\n',17,217)
add 		LexToken(ADD,'add',18,219)
j 		LexToken(REGISTER,'j',18,223)
, 		LexToken(COMMA,',',18,224)
1 		LexToken(NUMBER,1,18,226)

		LexToken(NEWLINE,'\n',18,227)
set 		LexToken(SET,'set',19,229)
PC 		LexToken(REGISTER,'PC',19,233)
, 		LexToken(COMMA,',',19,235)
nextchar 		LexToken(TEXT,'nextchar',19,237)


		LexToken(NEWLINE,'\n\n',19,245)
:setcolor 		LexToken(LABEL,':setcolor',21,247)

		LexToken(NEWLINE,'\n',21,256)
set 		LexToken(SET,'set',22,258)
b 		LexToken(REGISTER,'b',22,262)
, 		LexToken(COMMA,',',22,263)
a 		LexToken(REGISTER,'a',22,265)

		LexToken(NEWLINE,'\n',22,266)
and 		LexToken(AND,'and',23,268)
b 		LexToken(REGISTER,'b',23,272)
, 		LexToken(COMMA,',',23,273)
0xff 		LexToken(HEXNUM,'0xff',23,275)

		LexToken(NEWLINE,'\n',23,279)
shl 		LexToken(SHL,'shl',24,281)
b 		LexToken(REGISTER,'b',24,285)
, 		LexToken(COMMA,',',24,286)
8 		LexToken(NUMBER,8,24,288)

		LexToken(NEWLINE,'\n',24,289)
ifg 		LexToken(IFG,'ifg',25,291)
a 		LexToken(REGISTER,'a',25,295)
, 		LexToken(COMMA,',',25,296)
0x1ff 		LexToken(HEXNUM,'0x1ff',25,298)

		LexToken(NEWLINE,'\n',25,303)
add 		LexToken(ADD,'add',26,309)
b 		LexToken(REGISTER,'b',26,313)
, 		LexToken(COMMA,',',26,314)
0x80 		LexToken(HEXNUM,'0x80',26,316)

		LexToken(NEWLINE,'\n',26,320)
add 		LexToken(ADD,'add',27,322)
i 		LexToken(REGISTER,'i',27,326)
, 		LexToken(COMMA,',',27,327)
1 		LexToken(NUMBER,1,27,329)

		LexToken(NEWLINE,'\n',27,330)
set 		LexToken(SET,'set',28,332)
PC 		LexToken(REGISTER,'PC',28,336)
, 		LexToken(COMMA,',',28,338)
nextchar 		LexToken(TEXT,'nextchar',28,340)


		LexToken(NEWLINE,'\n\n',28,348)
:end 		LexToken(LABEL,':end',30,350)

		LexToken(NEWLINE,'\n',30,354)
set 		LexToken(SET,'set',31,356)
PC 		LexToken(REGISTER,'PC',31,360)
, 		LexToken(COMMA,',',31,362)
end 		LexToken(TEXT,'end',31,364)


		LexToken(NEWLINE,'\n\n',31,367)
:data 		LexToken(LABEL,':data',33,369)

		LexToken(NEWLINE,'\n',33,374)
dat 		LexToken(TEXT,'dat',34,376)
0x170 		LexToken(HEXNUM,'0x170',34,380)
, 		LexToken(COMMA,',',34,385)
Hello  		LexToken(STRING,'Hello ',34,387)
, 		LexToken(COMMA,',',34,395)
0x2e1 		LexToken(HEXNUM,'0x2e1',34,397)
, 		LexToken(COMMA,',',34,402)
world 		LexToken(STRING,'world',34,404)
, 		LexToken(COMMA,',',34,411)
0x170 		LexToken(HEXNUM,'0x170',34,413)
, 		LexToken(COMMA,',',34,418)
, how are you? 		LexToken(STRING,', how are you?',34,420)
----------------------> Done
